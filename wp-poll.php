<?php
/**
 * Plugin Name: Wordpress Poll
 * Description: Plugin for polling a survey
 * Plugin URI:https://www.commandmedia.net/
 * Author: Mukh. Kurniawan
 * Author URI: https://www.commandmedia.net/
 * Version: 1.0.0
 * License: GNU General Public License v2.0
 * Text Domain: wp-poll
 *
 * @package wp-poll
 */